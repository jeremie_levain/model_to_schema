<?php

/////* LITTLE FUNCTIONS */////

// We get the name and the type of a property.
function getProperty($property) {
    $property = preg_split("#[\s*@$\[\]]+#", $property);
    $property = preg_grep("#property#", $property, PREG_GREP_INVERT);
    return $property;
}

// We write the name, the description of a property.
function writeDescription($schema, $property, $modelname) {
    file_put_contents($schema, "  $property[3]:\n", FILE_APPEND | LOCK_EX);
    $propertyCleaned = preg_replace("#_|_at#", " ", $property[3]);

    if (preg_match("#_at#", $property[3])) {
        file_put_contents($schema, "    description: The date/time when the object was $propertyCleaned.\n", FILE_APPEND | LOCK_EX);
    }
    file_put_contents($schema, "    description: The $modelname $propertyCleaned", FILE_APPEND | LOCK_EX);

    if (preg_match("#im#", $property[3])) {
        file_put_contents($schema, " url", FILE_APPEND | LOCK_EX);
    }
}

// We write the type of a property.
function writeType($schema, $property) {
    if (preg_match("#null#", $property[2])) {
        $type = preg_split("#\|#", $property[2])[0];
        file_put_contents($schema,".\n    type: $type\n    nullable: true\n",FILE_APPEND | LOCK_EX);

    } else {
        file_put_contents($schema, ".\n    type: $property[2]\n", FILE_APPEND | LOCK_EX);
    }
}

// We write the examples of a property.
function writeExample($schema, $property) {
    if (preg_match("#int#", $property[2])) {
        file_put_contents($schema,"    example: 42\n",FILE_APPEND | LOCK_EX);

    } elseif (preg_match("#stri#", $property[2])) {
        if (preg_match("#slug#", $property[3])) {
            file_put_contents($schema,"    example: lorem-ipsum\n",FILE_APPEND | LOCK_EX);

        } elseif (preg_match("#im#", $property[3])) {
            file_put_contents($schema,"    example: https://cdn.mathrix.fr/categories/lorem-ipsum.png\n",FILE_APPEND | LOCK_EX);

        } else {
            file_put_contents($schema,"    example: lorem ipsum\n",FILE_APPEND | LOCK_EX);
        }

    } elseif (preg_match("#bool#", $property[2])) {
        file_put_contents($schema,"    example: True\n",FILE_APPEND | LOCK_EX);

    } elseif (preg_match("#mix#", $property[2])) {
        file_put_contents($schema,"    enum:\n      - \n",FILE_APPEND | LOCK_EX);

    } elseif (preg_match("#array#", $property[2])) {
        file_put_contents($schema,"      items:\n        type:\n",FILE_APPEND | LOCK_EX);

    } elseif (preg_match("#_at#", $property[3])) {
        file_put_contents($schema,"    format: date-time\n",FILE_APPEND | LOCK_EX);
    }
}


/////* PROCESSING OF ONE FILE */////
function convertModelToSchema($repertoryModels, $repertorySchemas, $model) {
    if (preg_match("#.php$#", $model)) {

        // We get the model name.
        $path = preg_split("#[/.]#", $model);
        $modelName = $path[sizeof($path) - 2];
        $schema = $repertorySchemas.'/'.$modelName.'.yaml';
        $modelname = strtolower($modelName);

        // We get the model properties in an array.
        $file = file_get_contents($repertoryModels.'/'.$model);
        $lines = preg_split("#\\n#", $file);
        $properties = preg_grep("#\s*\s@property\s[a-z|]*\s[a-z]*#", $lines);

        // We write the new schema.
        file_put_contents($schema, "description: $modelName\ntype: object\nproperties:\n",LOCK_EX);

        foreach ($properties as $property) {
            $property = getProperty($property);

            if (!preg_match("#link|ur[il]#", $property[3])) {
                writeDescription($schema, $property, $modelname);
                writeType($schema, $property);
                writeExample($schema, $property);
            }
        }
        return TRUE;
    }
    echo "Error: Model not found.\nModel seems to \"Model.php\"\n";
    return FALSE;
}


/////* PROCESSING A REPERTORY OF MODELS */////

function convertModelsToSchemasAll($repertoryModels, $repertorySchemas) {
    $models = scandir($repertoryModels);

    foreach ($models as $model) {
        convertModelToSchema($repertoryModels, $repertorySchemas, $model);
    }
}

function convertModelsToSchemasAuto($repertoryModels, $repertorySchemas) {
    $models = scandir($repertoryModels);
    $schemas = scandir($repertorySchemas);

    foreach ($models as $model) {

        // We get the model name.
        $modelName = preg_split("#\.#", $model)[0];
        $modelName = $modelName.'.yaml';

        if (!in_array($modelName, $schemas)) {
            convertModelToSchema($repertoryModels, $repertorySchemas, $model);
        }
    }
}


////////////////////////////* MAIN *////////////////////////////////

$repertoryModels = "Models";
$repertorySchemas = "schemas";

convertModelsToSchemasAuto($repertoryModels, $repertorySchemas);

//convertModelsToSchemasAll($repertoryModels, $repertorySchemas);

//convertModelToSchema($repertoryModels, $repertorySchemas, $model);